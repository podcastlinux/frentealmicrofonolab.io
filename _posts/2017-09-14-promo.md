---
layout: post  
title: "Promo Frente al Micrófono"  
date: 2017-09-14  
categories: podcast  
image: images/logo.png  
podcast_link: https://archive.org/download/PromoFAM/PromoFAM
tags: [audio, promo, Frente al Micrófono]  
comments: true 
---
Aquí puedes añadir las notas del programa 

<audio controls>
  <source src="https://archive.org/download/PromoFAM/PromoFAM.ogg" type="audio/ogg">
  <source src="https://archive.org/download/PromoFAM/PromoFAM.mp3" type="audio/mpeg">
</audio>

Añade imágenes a tu carpeta images y publícalas en el post de esta manera.  
~~~
![#Promo]({{ site.baseurl }}/images/promo.png)
~~~

Recuerda que puedes **contactar** con nosotros de las siguientes formas:

+ Twitter: <https://twitter.com/frenteal_micro>
+ Correo: <frentealmicrofono@disroot.com>
+ Web: <https://frentealmicrofono.gitlab.io/>
+ Telegram: <https://t.me/frentealmicrofono>
+ Youtube: <https://www.youtube.com/frentealmicrofono>
+ Feed Podcast: <https://frentealmicrofono.gitlab.io/feed>

